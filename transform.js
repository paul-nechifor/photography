const fs = require('fs');
const mkdirp = require('mkdirp');
const sharp = require('sharp');
const util = require('util');
const yaml = require('yaml');
const childProcess = require('child_process');

const exec = util.promisify(childProcess.exec);

const distImages = "dist/images";

const data = yaml.parse(fs.readFileSync('data.yaml', 'utf8'));

async function main() {
  checkNames();

  await mkdirp(distImages);

  data.images.forEach(async image => {
    const {
      name,
      path,
      quality = 90,
      size = 1800,
    } = image;

    const imagePath = `${distImages}/${name}.jpg`;
    const thumbPath = `${distImages}/${name}.thumb.jpg`;
    await writeImage(path, imagePath, size, quality);
    await writeImage(path, thumbPath, 400, quality);
  });
}

async function writeImage(src, dst, size, quality) {
  await sharp(src)
    .resize(size)
    .sharpen()
    .jpeg({quality, optimizeScans: true})
    .toFile(dst);
  let ret = await exec(`exiftool -all= '${dst}'`);
  console.log(ret.stdout, ret.stderr);
  ret = await exec(`exiftool -author="Paul Nechifor <paul@nechifor.net>" -copyright="Paul Nechifor (nechifor.net/photography)" -overwrite_original '${dst}'`);
  console.log(ret.stdout, ret.stderr);
}

function checkNames() {
  const seen = {};

  data.images.forEach(image => {
    if (image.name in seen) {
      throw new Exception(`Image with name'${image.name}' exists.`)
    }
    seen[image.name] = true;
  });
}

main();
